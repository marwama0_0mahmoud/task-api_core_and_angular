﻿using System.Threading.Tasks;
using Task.Dto;
using Task.Models;

namespace Task.Services
{
    public interface IAuthService
    {
        Task<AuthModel> RegisterAsync(RegisterModel model, bool isAdmin);
        Task<AuthModel> GetTokenAsync(TokenRequestModel model);
        Task<AuthModel> ResetPasswordAsync(ResetPasswordModel model);
    }
}