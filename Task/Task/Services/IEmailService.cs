﻿using System.Threading.Tasks;
using Task.Configuration;

namespace Task.Services
{
    public interface IEmailService
    {
        public bool SendMessageToConfirmEmail(UserData userData);
    }
}
