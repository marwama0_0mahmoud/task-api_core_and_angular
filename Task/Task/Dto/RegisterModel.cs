﻿using System.ComponentModel.DataAnnotations;

namespace Task.Dto
{
    public class RegisterModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }

        public string Password { get; set; }
        public string ConfirmPassword { get; set; }

    }
}