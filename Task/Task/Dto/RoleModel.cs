﻿using System.ComponentModel.DataAnnotations;

namespace Task.Dto
{
    public class RoleModel
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}